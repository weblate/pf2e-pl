Hooks.on("init", () => {
  if (typeof Babele !== "undefined") {
    Babele.get().register({
      module: "pf2e-pl",
      lang: "pl",
      dir: "translation/pl/compendium",
    });
  }
});
